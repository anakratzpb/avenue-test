require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'

#Capybara.include Capybara::DSL 
#Capybara.exact = true

# Instancia/Registra o driver do Selenium.
#Capybara.register_driver :selenium do |app|
 # Capybara::Selenium::Driver.new(app,:browser => :chrome,timeout: 30)
 #end

Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  #config.javascript_driver = :selenium_chrome
  config.default_max_wait_time = 30
  config.app_host = "https://qa-test.avenuecode.com/"
end
