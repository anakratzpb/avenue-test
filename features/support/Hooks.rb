Before do
  visit('/')

  @login_page = LoginPage.new
  @home_page = HomePage.new
  @task_page = TaskPage.new
  @subtask_page = SubTaskPage.new
  @header_page = HeaderPage.new
end

After do
  @login_page.sign_out
end