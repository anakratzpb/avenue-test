class TaskPage
  include Capybara::DSL

  def gettask_name_field
    find('.ng-scope ng-binding editable editable-click editable-empty')
  end

  def gettask_line
    find('tr[ng-repeat="task in tasks"]')
  end
   
  def getmanage_subtask_btn
    find('button[class="btn btn-xs btn-primary ng-binding"]')
  end

  def getsave_task_btn
    find('.btn btn-primary')
  end

  def click_save_task_btn
    find('.btn btn-primary').click
  end

  def click_create_task_btn
    find('span[ng-click="addTask()"]').click
  end

  def create_task(name)
    find('span[ng-click="addTask()"]').click
    using_wait_time 20 do
      rename = find('a[class="ng-scope ng-binding editable editable-click editable-empty"]')
      rename.click
      input = find(:xpath, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[2]/form/div/input')
      input.set(name)
      find(:xpath, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[2]/form/div/span/button[1]').click
    end
  end
end
