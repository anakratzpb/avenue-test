class HeaderPage
    include Capybara::DSL

    def fetch_my_task_link
        find(:xpath, '/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a')
    end

    def fetch_nav_bar
        find(:xpath, '/html/body/div[1]/div[1]/div/div[2]')
    end 

    def click_my_task_link
        find(:xpath, '/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a').click
    end
end
