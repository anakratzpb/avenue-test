class LoginPage
    include Capybara::DSL

    def goto_SignIn_page
      visit('https://qa-test.avenuecode.com/users/sign_in')
    end

    def sign_in(email, password)
      fill_in 'user_email', with: email
      fill_in 'user_password', with: password
      find('input[value="Sign in"]').click
    end

    def sign_out
      find('a[href="/users/sign_out"]').click
    end

end
