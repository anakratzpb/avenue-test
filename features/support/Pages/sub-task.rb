class SubTaskPage
  include Capybara::DSL

  def getsub_task_number
    find('button[class="btn btn-xs btn-primary ng-binding"].text')
  end

  def click_manage_subtask_btn
    find('button[class="btn btn-xs btn-primary ng-binding"]').click
  end

  def click_add_subtask_btn
    find('button[id="add-subtask"]').click
  end

  def getediting_task_modal
    find('div[class="modal-dialog"]')
  end

  def gettask_desc
    find('textarea[edit_task]')
  end

  def getdue_date_field
    find('input[id="dueDate"]')
  end

  def fill_task_desc(text)
    fill_in 'div[class="modal-dialog"]', with: text 
  end

  def fill_due_date(date)
    fill_in 'input[id="dueDate"]', with: date
  end

  def create_subtask(name)
    self.click_manage_subtask_btn
    self.fill_task_desc(name)
    self.fill_due_date('10/10/2020')
    self.click_add_subtask_btn
  end

  def create_subtask_with_blank_desc_field
    self.click_manage_subtask_btn
    self.fill_due_date('10/10/2020')
    self.click_add_subtask_btn
  end

  def create_subtask_with_blank_duedate(name)
    self.click_manage_subtask_btn
    self.fill_task_desc(name)
    self.fill_due_date(' ')
    self.click_add_subtask_btn
  end

  def create_subtask_with_blank_fields
    self.click_manage_subtask_btn
    self.fill_task_desc(' ')
    self.fill_due_date(' ')
    self.click_add_subtask_btn
  end
end