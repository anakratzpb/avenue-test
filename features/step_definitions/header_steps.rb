When("I see the header section") do
  @header_page.fetch_nav_bar
end
  
Then("I should see link My Tasks inside the header section") do  
  expect(page).to have_content(@header_page.fetch_my_task_link)
end
  
When("I click on the My Tasks link") do
  @header_page.click_my_task_link
end
  
Then("I should be taken to the tasks page") do
  expect(page).to have_current_path('https://qa-test.avenuecode.com/tasks', url: true)
end
  
Then("I should see a {string}") do |label|
  label = 'Hey Ana, this is your todo list for today:'
  expect(page).to have_content(label)
end