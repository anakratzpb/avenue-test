When("I go to my task page") do
  @home_page.goto_my_tasks_page
end
  
Then("I should see the Manage Subtask button") do
  page.has_content?(@task_page.getmanage_subtask_btn, :visible => true)
end
  
Then("I should see the numbers of subtask created in the button label") do
  page.has_content?(@subtask_page.getsub_task_number, :visible => true)
end
  
When("I click on the Manage Subtask button") do
  @subtask_page.click_manage_subtask_btn
end
  
Then("I should see a modal dialog be opened") do
  page.has_content?(@subtask_page.getediting_task_modal, visible: true)
end
  
Then("I should see a read-only field with task id and description") do
  page.has_content?(@subtask_page.gettask_desc, :text => 'test1')
end
  
Then("I should see Subtask description field") do
  page.has_content?(@subtask_page.gettask_desc)
end
  
Then("I should see a due date field in the correct format") do
  page.has_css?(@subtask_page.getdue_date_field, :placeholder => "MM/dd/yyyy")
end
  
When("I type two hundred fifty characters inside SubTask description") do
  text = 'abcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcde'
  @subtask_page.create_subtask(text)
end
  
Then("I should be able to add a Subtask") do
  page.has_text?(self.text)
end
  
When("I try to type more than two hundred characters in subtask description") do
  text = 'abcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdef'
  @subtask_page.create_subtask(text)
end
  
Then("I should not be able to add more than two hundred characters on the field") do
  page.has_text?('error') #?
end
  
When("I leave subtask description field blank") do
  @subtask_page.create_subtask_with_blank_desc_field
end
  
Then("I should not be able to add subtask") do
  page.has_text?('error') #?
end
  
When("I leave due date field blank") do
  @subtask_page.create_subtask_with_blank_duedate('test due date incorrect')
end
  
When("I leave subtask description and due date fields blank") do
    @subtask_page.create_subtask_with_blank_fields
end