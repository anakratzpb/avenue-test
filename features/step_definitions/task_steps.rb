Given("I am logged on ToDo application") do
    @login_page.goto_SignIn_page
    @login_page.sign_in('anakratz.pb@gmail.com', 'Admin123')
end
  
  When("I click to create a new Task") do
    @home_page.goto_my_tasks_page
    @task_page.create_task('task 1')
  end
  
  Then("I should see task created on the tasl list") do
    page.has_content?(@task_page.gettask_line, 'test 1')
  end


  When("I enter the less than three characters on the task name") do
    @task_page.create_task('ta')
  end
  
  Then("I should not be able to set a task name") do
    page.has_text('error') #? não especificado mensagem que deve aparecer
    page.has_no_text?(@task_page.gettask_line, 'test 1')
  end
  

  When("I enter more than two hundred fifty characters on the task name") do
    text = 'abcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdefghijklmnoprstuvwyzabcdef'
    @task_page.create_task(text)
  end