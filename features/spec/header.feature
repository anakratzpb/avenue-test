Feature: header

   Feature description for header section

@header
Scenario: Verify My Tasks link presence on NavBar
    Given I am logged on ToDo application
    When I see the header section
    Then I should see link My Tasks inside the header section

    When I click on the My Tasks link
    Then I should be taken to the tasks page 
    And I should see a "Hey {user}, this is your todo list for today:"

