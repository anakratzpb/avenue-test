Feature: task

   Feature description for Tasks

@task
Scenario: Create Task
    Given I am logged on ToDo application
    When I click to create a new Task
    Then I should see task created on the tasl list

    When I enter the less than three characters on the task name
    Then I should not be able to set a task name

    When I enter more than two hundred fifty characters on the task name
    Then I should not be able to set a task name  