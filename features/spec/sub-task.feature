Feature: sub-task

   Feature description for Sub-Tasks

@subtask
Scenario: Verify the presence of Manage Subtask link
    Given I am logged on ToDo application
    When I go to my task page
    Then I should see the Manage Subtask button
    And I should see the numbers of subtask created in the button label

    When I click on the Manage Subtask button
    Then I should see a modal dialog be opened
    And I should see a read-only field with task id and description
    And I should see Subtask description field
    And I should see a due date field in the correct format
    
    When I type two hundred fifty characters inside SubTask description
    Then I should be able to add a Subtask
    
    When I try to type more than two hundred characters in subtask description
    Then I should not be able to add more than two hundred characters on the field

    When I add a valid due date
    But I leave subtask description field blank
    Then I should not be able to add subtask

    When I leave due date field blank
    Then I should not be able to add subtask

    When I leave subtask description and due date fields blank
    Then I should not be able to add a subtask



